provider "aws" {
  region = "ca-central-1"
}


resource "aws_instance" "my_Ubuntu" {
  ami           = data.aws_ami.ubuntu_latest.id
  instance_type = "t2.micro"

  tags = {
    Name    = "Any Ubuntu"
    Owner   = "TFTP"
    Project = "AWS Learning"
  }
}


# Используя DataSource ищем последнюю версию Ubuntu 20.04 Server
data "aws_ami" "ubuntu_latest" {
  owners      = ["099720109477"]
  most_recent = true # Означает самый последний
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Используя DataSource ищем последнюю версию Amazon Linux
data "aws_ami" "amazon_linux" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }
}

output "latest_ubuntu_ami_id" {
  value = data.aws_ami.ubuntu_latest.id
}

output "latest_ubuntu_ami_name" {
  value = data.aws_ami.ubuntu_latest.name
}

output "latest_amazon_ami_id" {
  value = data.aws_ami.amazon_linux.id
}

output "latest_amazon_ami_name" {
  value = data.aws_ami.amazon_linux.name
}

