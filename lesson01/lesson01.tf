provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "my_Ubuntu" {
  count = 2 # Важнейший параметр, позволяет создавать несколько одинаковых ресурсов
  ami = "ami-042ad9eec03638628"
  instance_type = "t2.micro"

  tags = {
    Name = "Any Ubuntu"
    Owner = "TFTP"
    Project = "AWS Learning"
  }
}
