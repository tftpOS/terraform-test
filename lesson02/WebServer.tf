provider "aws" {
    region = "eu-central-1"
}

resource "aws_instance" "my_webserver" {
    ami = "ami-04c921614424b07cd" # Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.my_web_server.id]
    # Далее user_data цепляет templatefile, в фигурных скобках передаются в него переменные
    user_data = templatefile("any_data.template.tpl", {
      f_name = "OS",
      l_name = "Tftp",
      names = ["Petya", "Jon", "Rebbeka", "Ivan"]
    })

  tags = {
    Name = "Web Server Build by Terraform"
    Owner = "TFTP"
  }
}

resource "aws_security_group" "my_web_server" {
  name        = "WebServer Security Group"
  description = "My First Security Group"

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Web Server Security Group"
    Owner = "TFTP"
  }
}
