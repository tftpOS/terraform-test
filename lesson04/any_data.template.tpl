#!/bin/bash
yum -y update
yum -y install httpd


myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`

cat <<EOF > /var/www/html/index.html
<html>
<h2>Build from Terraform</h2>
<p>Owner ${f_name} ${l_name}</p>

%{ for x in names ~}
Hello to ${x} from ${f_name}<br>
%{ endfor ~}
</html>
EOF

sudo service httpd start
chkconfig httpd on
