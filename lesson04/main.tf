provider "aws" {
  region = "eu-central-1"
}

resource "aws_eip" "my_static_ip" {
  instance = aws_instance.my_webserver.id
}

resource "aws_instance" "my_webserver" {
  ami                    = "ami-04c921614424b07cd" # Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_web_server.id]
  # Далее user_data цепляет templatefile, в фигурных скобках передаются в него переменные
  user_data = templatefile("any_data.template.tpl", {
    f_name = "OS",
    l_name = "Tftp",
    names  = ["Petya", "Jon", "Rebbeka", "Ivan", "Mary", "OS"]
  })

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name  = "Web Server Build by Terraform"
    Owner = "TFTP"
  }
}

resource "aws_security_group" "my_web_server" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web Server Security Group"
    Owner = "TFTP"
  }
}
