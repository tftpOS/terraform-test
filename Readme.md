# Terraform

Первая команда инициализации, в которой просматриваются файлы и докачиваются плагины
```sh
terraform init
```
далее команда которая показывает, что терраформ собирается делать, план:
```sh
terraform plan
```
далее команда создания по этому плану:
```sh
terraform apply
```
Чтобы не хранить в коде секретные данные, нужно в терминале ввести команду,
которая определить переменные окружения с нашими секретными данными:
```sh
export AWS_ACCESS_KEY_ID=xxxxxxxxx
export AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxx
```
Регион тоже можно прописать в переменных окружения командой:
```sh
export AWS_DEFAULT_REGION=eu-central-1
```
Удаление всех инстансов:
```sh
terraform destroy
```
Посмотреть [доступные регионы aws](https://awsregion.info/). 

Если надо найти какие нибудь настройки, например aws_security_group,
нужно в поиск забить `terraform aws_security_group` и получим то, что нам нужно

Чтобы протестировать, как сработает та или иная функция в терраформ можно использовать команду `terraform console`
и далее в появившейся консоле запускать нужную функцию с данными и смотреть что выводится

Чтобы нельзя было "случайно" убить сервер, при пересборке конфигурации, используется вот такой параметр:
```
  lifecycle {
    prevent_destroy = true
  }
```
`Или` указать какие изменения не применять:
```
  lifecycle {
    ignore_changes = ["ami", "user_data"]
  }
```
Ещё один lifecycle, который уменьшает время разворчивания исправлений сервера (почти Zero Down Time):
- создается aws_eip ресурс со статическим ip и  привязкой к нашему инстансу
```
resource "aws_eip" "my_static_ip" {
  instance = aws_instance.my_webserver.id
}
```
- в lifecycle указывается, что прежде убийства ресурса нужно создать новый
```
  lifecycle {
    create_before_destroy = true
  }
```
## Output
Создается отдельным разделом, где value присваиваем нужный нам параметр:
```
output "webserver_public_ip" {
  value = aws_eip.my_static_ip.public_ip
}
```
Чтобы узнать какой аттрибут можно вывести для aws_eip, можно посмотреть в [документацию](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip)

Также, если рессурс уже создан, то посмотреть `output` можно командой `terraform output`

А команда `terraform show` может показать, какие аттрибуты могут понадобиться и, соответственно, быть использованны в output.

Обычно раздел `output` переносят в отдельный файл, terraform сам объеденит файлы .tf, главное чтоб они были в одной дирректории.

## Зависимости
Если инстансов несколько тогда, чтобы указать какой ресурс поднять в первую очередь, нужно использовать параметр инстанса:  `depends_on = [aws_instance.my_server_up_1]`, и инстанс поднимится только после зависимости.

## Data Source
Используется для сбора данных, необходимых для создания рессурсов или другого анализа.

Data source для aws можно посмотреть в [документации](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zone)

Наиболее используемые:
- aws_availability_zones
- aws_caller_identity
- aws_region
- aws_vpc


