#----------------------------------------------------------
# Provision Highly Availabe Web in any Region Default VPC
# Create:
#    - Security Group for Web Server
#    - Launch Configuration with Auto AMI Lookup
#    - Auto Scaling Group using 2 Availability Zones
#    - Classic Load Balancer in 2 Availability Zones
#
# Thanks for Denis Astahov (https://www.youtube.com/watch?v=xRuG2WDACK8&list=PLg5SS_4L6LYujWDTYb-Zbofdl44Jxb2l8&index=18)
#-----------------------------------------------------------

provider "aws" {
  region = "eu-central-1"
}


data "aws_availability_zones" "available" {}
data "aws_ami" "latest_amazon_linux" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

#--------------------------------------------------------------
# Определяем security group
#--------------------------------------------------------------
resource "aws_security_group" "web" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["80", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Dynamic SecurityGroup"
    Owner = "TFTP"
  }
}

#-------------------------------------------------
# Определяем aws_launch_configuration который будет использоваться для инстанса
#-------------------------------------------------

resource "aws_launch_configuration" "web" {
  # name не подходит, потому что должно быть разным всякий раз при создании
  # поэтому используется name_prefix
  //  name            = "WebServer-Highly-Available-LC"
  name_prefix     = "WebServer-Highly-Available-LC-"
  image_id        = data.aws_ami.latest_amazon_linux.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web.id]
  user_data       = file("user_data.sh")

  lifecycle {
    create_before_destroy = true
  }
}


#---------------------------------------------------
# Создается autoscaling_group с уникальным именем
#---------------------------------------------------
resource "aws_autoscaling_group" "web" {
  name = "ASG-${aws_launch_configuration.web.name}"
  # указываем откуда берем конфигурацию
  launch_configuration = aws_launch_configuration.web.name
  # количество инстансов
  min_size          = 2
  max_size          = 2
  min_elb_capacity  = 2
  health_check_type = "ELB"
  # указываем (подсети для зон) зоны vpc, обычно указывают первые две, как более используемые
  vpc_zone_identifier = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  # указываем elb
  load_balancers = [aws_elb.web.name]

  dynamic "tag" {
    for_each = {
      Name   = "WebServer in ASG"
      Owner  = "Denis Astahov"
      TAGKEY = "TAGVALUE"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

#-----------------------------------------
# Прописываем load balancer
#-----------------------------------------
resource "aws_elb" "web" {
  name               = "WebServer-HA-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }
  tags = {
    Name = "WebServer-Highly-Available-ELB"
  }
}

#------------------------------------
# Создание подсети по умолчанию для зон
#------------------------------------
resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

#--------------------------------------------------
# Вывод DNS имени load balancer по которому можно обращаться
#-------------------------------------------------- 
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}
